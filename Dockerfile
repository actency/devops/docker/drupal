FROM actency/docker-apache-php:latest

# Apache2 config
COPY config/apache2.conf /etc/apache2

# Install drupal binaries
RUN cd /var/www && \
    composer create-project drupal/recommended-project drupal

# Set correct folder permissions
RUN chown -R web:www-data /var/www/drupal
